
package com.edsd.royalshirt.restapidto;

public class Unit {
    private Integer unitId;
    private Integer content;

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getContent() {
        return content;
    }

    public void setContent(Integer content) {
        this.content = content;
    }
    
}
