package com.edsd.royalshirt.restapidto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ItemImage {
    private String id;
    private String url;
    private String uploadUrl;
    List<Name> names = new ArrayList();
    List<Availabilities> availabilities = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public List<Name> getNames() {
        List<Name> result = new ArrayList();
        result.addAll(names);
        return result;
    }

    public void setNames(List<Name> names) {
        this.names = names;
    }

    public List<Availabilities> getAvailabilities() {
        List<Availabilities> result = new ArrayList();
        result.addAll(availabilities);
        return result;
    }

    public void setAvailabilities(List<Availabilities> availabilities) {
        this.availabilities = availabilities;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemImage other = (ItemImage) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        return true;
    }
    
}
