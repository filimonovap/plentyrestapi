package com.edsd.royalshirt.restapidto;

public class Category {
    private Integer variationId;
    private Integer categoryId;
    private Integer branchId;
    private String manually;
    private Integer position;
    private Integer plentyId;

    public Integer getVariationId() {
        return variationId;
    }

    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    
    public Integer getBranchId() {
        return branchId;
    }

    public void setBranchId(Integer branchId) {
        this.branchId = branchId;
    }

    public String getManually() {
        return manually;
    }

    public void setManually(String manually) {
        this.manually = manually;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getPlentyId() {
        return plentyId;
    }

    public void setPlentyId(Integer plentyId) {
        this.plentyId = plentyId;
    }
}
