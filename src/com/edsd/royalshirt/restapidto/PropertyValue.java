package com.edsd.royalshirt.restapidto;

import java.util.ArrayList;
import java.util.List;

public class PropertyValue {
    private Integer id;
    private Integer propertyId;
    private Integer variationId;
    private Integer propertySelectionId;
    private Integer valueInt;
    private Float valueFloat;
    private List<ValueText> valueTexts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getVariationId() {
        return variationId;
    }

    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    public Integer getPropertySelectionId() {
        return propertySelectionId;
    }

    public void setPropertySelectionId(Integer propertySelectionId) {
        this.propertySelectionId = propertySelectionId;
    }

    public Integer getValueInt() {
        return valueInt;
    }

    public void setValueInt(Integer valueInt) {
        this.valueInt = valueInt;
    }

    public Float getValueFloat() {
        return valueFloat;
    }

    public void setValueFloat(Float valueFloat) {
        this.valueFloat = valueFloat;
    }

    public List<ValueText> getValueTexts() {
        List<ValueText> result = new ArrayList();
        result.addAll(valueTexts);
        return result;
    }

    public void setValueTexts(List<ValueText> valueTexts) {
        this.valueTexts = valueTexts;
    }
}
