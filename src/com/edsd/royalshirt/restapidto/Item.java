package com.edsd.royalshirt.restapidto;

import java.util.ArrayList;
import java.util.List;

public class Item {
    private Integer id;
    private Integer position;
    private Integer manufacturerId;
    private Integer stockType;
    private Integer storeSpecial;
    private Integer condition;
    private String amazonFedas;
    private String customsTariffNumber;
    private Integer producingCountryId;
    private Integer revenueAccount;
    private Integer couponRestriction;
    private Integer flagOne;
    private Integer flagTwo;
    private Integer ageRestriction;
    private Integer amazonProductType;
    private Integer ebayPresetId;
    private Integer ebayCategory;
    private Integer ebayCategory2;
    private Integer ebayStoreCategory;
    private Integer ebayStoreCategory2;
    private Integer amazonFbaPlatform;
    private Integer feedback;
    private boolean isSubscribable;
    private Integer rakutenCategoryId;
    private Integer conditionApi;
    private boolean isShippableByAmazon;
    private Integer ownerId;
    private String itemType;
    private Integer mainVariationId;
    private List<ItemVariant> variations;

    public Item() {
    }
    
    public Item(Item item){
        this.id = item.id;
        this.position = item.position;
        this.manufacturerId = item.manufacturerId;
        this.stockType = item.stockType;
        this.storeSpecial = item.storeSpecial;
        this.condition = item.condition;
        this.amazonFedas = item.amazonFedas;
        this.customsTariffNumber = item.customsTariffNumber;
        this.producingCountryId = item.producingCountryId;
        this.revenueAccount = item.revenueAccount;
        this.couponRestriction = item.couponRestriction;
        this.flagOne = item.flagOne;
        this.flagTwo = item.flagTwo;
        this.ageRestriction = item.ageRestriction;
        this.amazonProductType = item.amazonProductType;
        this.ebayPresetId = item.ebayPresetId;
        this.ebayCategory = item.ebayCategory;
        this.ebayCategory2 = item.ebayCategory2;
        this.ebayStoreCategory = item.ebayStoreCategory;
        this.ebayStoreCategory2 = item.ebayStoreCategory2;
        this.amazonFbaPlatform = item.amazonFbaPlatform;
        this.feedback = item.feedback;
        this.isSubscribable = item.isSubscribable;
        this.rakutenCategoryId = item.rakutenCategoryId;
        this.conditionApi = item.conditionApi;
        this.isShippableByAmazon = item.isShippableByAmazon;
        this.ownerId = item.ownerId;
        this.itemType = item.itemType;
        this.mainVariationId = item.mainVariationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Integer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Integer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public Integer getStockType() {
        return stockType;
    }

    public void setStockType(Integer stockType) {
        this.stockType = stockType;
    }

    public Integer getStoreSpecial() {
        return storeSpecial;
    }

    public void setStoreSpecial(Integer storeSpecial) {
        this.storeSpecial = storeSpecial;
    }

    public Integer getCondition() {
        return condition;
    }

    public void setCondition(Integer condition) {
        this.condition = condition;
    }

    public String getAmazonFedas() {
        return amazonFedas;
    }

    public void setAmazonFedas(String amazonFedas) {
        this.amazonFedas = amazonFedas;
    }

    public String getCustomsTariffNumber() {
        return customsTariffNumber;
    }

    public void setCustomsTariffNumber(String customsTariffNumber) {
        this.customsTariffNumber = customsTariffNumber;
    }

    public Integer getProducingCountryId() {
        return producingCountryId;
    }

    public void setProducingCountryId(Integer producingCountryId) {
        this.producingCountryId = producingCountryId;
    }

    public Integer getRevenueAccount() {
        return revenueAccount;
    }

    public void setRevenueAccount(Integer revenueAccount) {
        this.revenueAccount = revenueAccount;
    }

    public Integer getCouponRestriction() {
        return couponRestriction;
    }

    public void setCouponRestriction(Integer couponRestriction) {
        this.couponRestriction = couponRestriction;
    }

    public Integer getFlagOne() {
        return flagOne;
    }

    public void setFlagOne(Integer flagOne) {
        this.flagOne = flagOne;
    }

    public Integer getFlagTwo() {
        return flagTwo;
    }

    public void setFlagTwo(Integer flagTwo) {
        this.flagTwo = flagTwo;
    }

    public Integer getAgeRestriction() {
        return ageRestriction;
    }

    public void setAgeRestriction(Integer ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    public Integer getAmazonProductType() {
        return amazonProductType;
    }

    public void setAmazonProductType(Integer amazonProductType) {
        this.amazonProductType = amazonProductType;
    }

    public Integer getEbayPresetId() {
        return ebayPresetId;
    }

    public void setEbayPresetId(Integer ebayPresetId) {
        this.ebayPresetId = ebayPresetId;
    }

    public Integer getEbayCategory() {
        return ebayCategory;
    }

    public void setEbayCategory(Integer ebayCategory) {
        this.ebayCategory = ebayCategory;
    }

    public Integer getEbayCategory2() {
        return ebayCategory2;
    }

    public void setEbayCategory2(Integer ebayCategory2) {
        this.ebayCategory2 = ebayCategory2;
    }

    public Integer getEbayStoreCategory() {
        return ebayStoreCategory;
    }

    public void setEbayStoreCategory(Integer ebayStoreCategory) {
        this.ebayStoreCategory = ebayStoreCategory;
    }

    public Integer getEbayStoreCategory2() {
        return ebayStoreCategory2;
    }

    public void setEbayStoreCategory2(Integer ebayStoreCategory2) {
        this.ebayStoreCategory2 = ebayStoreCategory2;
    }

    public Integer getAmazonFbaPlatform() {
        return amazonFbaPlatform;
    }

    public void setAmazonFbaPlatform(Integer amazonFbaPlatform) {
        this.amazonFbaPlatform = amazonFbaPlatform;
    }

    public Integer getFeedback() {
        return feedback;
    }

    public void setFeedback(Integer feedback) {
        this.feedback = feedback;
    }

    public boolean isIsSubscribable() {
        return isSubscribable;
    }

    public void setIsSubscribable(boolean isSubscribable) {
        this.isSubscribable = isSubscribable;
    }

    public Integer getRakutenCategoryId() {
        return rakutenCategoryId;
    }

    public void setRakutenCategoryId(Integer rakutenCategoryId) {
        this.rakutenCategoryId = rakutenCategoryId;
    }

    public Integer getConditionApi() {
        return conditionApi;
    }

    public void setConditionApi(Integer conditionApi) {
        this.conditionApi = conditionApi;
    }

    public boolean isIsShippableByAmazon() {
        return isShippableByAmazon;
    }

    public void setIsShippableByAmazon(boolean isShippableByAmazon) {
        this.isShippableByAmazon = isShippableByAmazon;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Integer getMainVariationId() {
        return mainVariationId;
    }

    public void setMainVariationId(Integer mainVariationId) {
        this.mainVariationId = mainVariationId;
    }

    public List<ItemVariant> getVariations() {
        List<ItemVariant> result = new ArrayList();
        result.addAll(variations);
        return result;
    }

    public void setVariations(List<ItemVariant> variations) {
        this.variations = variations;
    }
    
}
