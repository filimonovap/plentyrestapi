package com.edsd.royalshirt.restapidto;

public class Client {
    private Integer plentyId;
    private Integer variantId;

    public Integer getPlentyId() {
        return plentyId;
    }

    public void setPlentyId(Integer plentyId) {
        this.plentyId = plentyId;
    }

    public Integer getVariantId() {
        return variantId;
    }

    public void setVariantId(Integer variantId) {
        this.variantId = variantId;
    }
}
