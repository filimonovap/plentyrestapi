package com.edsd.royalshirt.restapidto;

public class SalesPrice {
    private Integer salesPriceId;
    private Double price;

    public Integer getSalesPriceId() {
        return salesPriceId;
    }

    public void setSalesPriceId(Integer salesPriceId) {
        this.salesPriceId = salesPriceId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
