package com.edsd.royalshirt.restapidto;

import com.edsd.royalshirt.restapi.AttributeValueNames;

public class AttributeValues {
    private Integer attributeId;
    private Integer valueId;

    public Integer getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    public Integer getValueId() {
        return valueId;
    }

    public void setValueId(Integer valueId) {
        this.valueId = valueId;
    }
    
    public String getValueName(){
        return AttributeValueNames.INSTANCE.getValueName(valueId.toString());
    }
}
