package com.edsd.royalshirt.restapidto;

import java.util.ArrayList;
import java.util.List;

public class ItemVariant {
    private Integer id;
    private Integer itemId;
    private String name;
    private Boolean isMain;
    private Integer mainVariationId;
    private Boolean isActive;
    private String number;
    private Integer position;
    private String model;
    private String externalId;
    private Integer availability;
    private String estimatedAvailableAt;
    private Float purchasePrice;
    private Integer priceCalculationId;
    private String picking;
    private Integer stockLimitation;
    private Boolean isVisibleIfNetStockIsPositive;
    private Boolean isInvisibleIfNetStockIsNotPositive;
    private Boolean isAvailableIfNetStockIsPositive;
    private Boolean isUnavailableIfNetStockIsNotPositive;
    private Integer mainWarehouseId;
    private Float maximumOrderQuantity;
    private Float minimumOrderQuantity;
    private Float intervalOrderQuantity;
    private Integer weightG;
    private Integer weightNetG;
    private Integer widthMM;
    private Integer lengthMM;
    private Integer heightMM;
    private Float extraShippingCharge1;
    private Float extraShippingCharge2;
    private Integer unitsContained;
    private Integer palletTypeId;
    private Integer packingUnits;
    private Integer packingUnitTypeId;
    private Float transportationCosts;
    private Float storageCosts;
    private Float customs;
    private Float operatingCosts;
    private Integer vatId;
    private String bundleType;
    private Integer automaticClientVisibility;
    private Boolean isHiddenInCategoryList;
    private Float defaultShippingCosts;
    private String availableUntil;
    private String releasedAt;
    private String plenty_item_variation_base_show_unit_price;
    private Unit unit;
    private List<AttributeValues> variationAttributeValues;
    private List<SalesPrice> variationSalesPrices;
    private List<Category> variationDefaultCategory;
    private List<Category> variationCategories;
    private List<Barcode> variationBarcodes;
    private List<Market> variationMarkets;
    private List<Client> variationClients;

    public ItemVariant() {
    }

    public ItemVariant(ItemVariant variant){
        this.id = variant.id;
        this.itemId = variant.itemId;
        this.name = variant.name;
        this.isMain = variant.isMain;
        this.mainVariationId = variant.mainVariationId;
        this.isActive = variant.isActive;
        this.number = variant.number;
        this.position = variant.position;
        this.model = variant.model;
        this.externalId = variant.externalId;
        this.availability = variant.availability;
        this.estimatedAvailableAt = variant.estimatedAvailableAt;
        this.purchasePrice = variant.purchasePrice;
        this.priceCalculationId = variant.priceCalculationId;
        this.picking = variant.picking;
        this.stockLimitation = variant.stockLimitation;
        this.isVisibleIfNetStockIsPositive = variant.isVisibleIfNetStockIsPositive;
        this.isInvisibleIfNetStockIsNotPositive = variant.isInvisibleIfNetStockIsNotPositive;
        this.isAvailableIfNetStockIsPositive = variant.isAvailableIfNetStockIsPositive;
        this.isUnavailableIfNetStockIsNotPositive = variant.isUnavailableIfNetStockIsNotPositive;
        this.mainWarehouseId = variant.mainWarehouseId;
        this.maximumOrderQuantity = variant.maximumOrderQuantity;
        this.minimumOrderQuantity = variant.minimumOrderQuantity;
        this.intervalOrderQuantity = variant.intervalOrderQuantity;
        this.weightG = variant.weightG;
        this.weightNetG = variant.weightNetG;
        this.widthMM = variant.widthMM;
        this.lengthMM = variant.lengthMM;
        this.heightMM = variant.heightMM;
        this.extraShippingCharge1 = variant.extraShippingCharge1;
        this.extraShippingCharge2 = variant.extraShippingCharge2;
        this.unitsContained = variant.unitsContained;
        this.palletTypeId = variant.palletTypeId;
        this.packingUnits = variant.packingUnits;
        this.packingUnitTypeId = variant.packingUnitTypeId;
        this.transportationCosts = variant.transportationCosts;
        this.storageCosts = variant.storageCosts;
        this.customs = variant.customs;
        this.operatingCosts = variant.operatingCosts;
        this.vatId = variant.vatId;
        this.bundleType = variant.bundleType;
        this.automaticClientVisibility = variant.automaticClientVisibility;
        this.isHiddenInCategoryList = variant.isHiddenInCategoryList;
        this.defaultShippingCosts = variant.defaultShippingCosts;
        this.availableUntil = variant.availableUntil;
        this.releasedAt = variant.releasedAt;
        this.plenty_item_variation_base_show_unit_price = variant.plenty_item_variation_base_show_unit_price;
    }        

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsMain() {
        return isMain;
    }

    public void setIsMain(boolean isMain) {
        this.isMain = isMain;
    }

    public Integer getMainVariationId() {
        return mainVariationId;
    }

    public void setMainVariationId(Integer mainVariationId) {
        this.mainVariationId = mainVariationId;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Integer getAvailability() {
        return availability;
    }

    public void setAvailability(Integer availability) {
        this.availability = availability;
    }

    public String getEstimatedAvailableAt() {
        return estimatedAvailableAt;
    }

    public void setEstimatedAvailableAt(String estimatedAvailableAt) {
        this.estimatedAvailableAt = estimatedAvailableAt;
    }

    public Float getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(Float purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public Integer getPriceCalculationId() {
        return priceCalculationId;
    }

    public void setPriceCalculationId(Integer priceCalculationId) {
        this.priceCalculationId = priceCalculationId;
    }

    public String getPicking() {
        return picking;
    }

    public void setPicking(String picking) {
        this.picking = picking;
    }

    public Integer getStockLimitation() {
        return stockLimitation;
    }

    public void setStockLimitation(Integer stockLimitation) {
        this.stockLimitation = stockLimitation;
    }

    public boolean isIsVisibleIfNetStockIsPositive() {
        return isVisibleIfNetStockIsPositive;
    }

    public void setIsVisibleIfNetStockIsPositive(boolean isVisibleIfNetStockIsPositive) {
        this.isVisibleIfNetStockIsPositive = isVisibleIfNetStockIsPositive;
    }

    public boolean isIsInvisibleIfNetStockIsNotPositive() {
        return isInvisibleIfNetStockIsNotPositive;
    }

    public void setIsInvisibleIfNetStockIsNotPositive(boolean isInvisibleIfNetStockIsNotPositive) {
        this.isInvisibleIfNetStockIsNotPositive = isInvisibleIfNetStockIsNotPositive;
    }

    public boolean isIsAvailableIfNetStockIsPositive() {
        return isAvailableIfNetStockIsPositive;
    }

    public void setIsAvailableIfNetStockIsPositive(boolean isAvailableIfNetStockIsPositive) {
        this.isAvailableIfNetStockIsPositive = isAvailableIfNetStockIsPositive;
    }

    public boolean isIsUnavailableIfNetStockIsNotPositive() {
        return isUnavailableIfNetStockIsNotPositive;
    }

    public void setIsUnavailableIfNetStockIsNotPositive(boolean isUnavailableIfNetStockIsNotPositive) {
        this.isUnavailableIfNetStockIsNotPositive = isUnavailableIfNetStockIsNotPositive;
    }

    public Integer getMainWarehouseId() {
        return mainWarehouseId;
    }

    public void setMainWarehouseId(Integer mainWarehouseId) {
        this.mainWarehouseId = mainWarehouseId;
    }

    public Float getMaximumOrderQuantity() {
        return maximumOrderQuantity;
    }

    public void setMaximumOrderQuantity(Float maximumOrderQuantity) {
        this.maximumOrderQuantity = maximumOrderQuantity;
    }

    public Float getMinimumOrderQuantity() {
        return minimumOrderQuantity;
    }

    public void setMinimumOrderQuantity(Float minimumOrderQuantity) {
        this.minimumOrderQuantity = minimumOrderQuantity;
    }

    public Float getIntervalOrderQuantity() {
        return intervalOrderQuantity;
    }

    public void setIntervalOrderQuantity(Float intervalOrderQuantity) {
        this.intervalOrderQuantity = intervalOrderQuantity;
    }

    public Integer getWeightG() {
        return weightG;
    }

    public void setWeightG(Integer weightG) {
        this.weightG = weightG;
    }

    public Integer getWeightNetG() {
        return weightNetG;
    }

    public void setWeightNetG(Integer weightNetG) {
        this.weightNetG = weightNetG;
    }

    public Integer getWidthMM() {
        return widthMM;
    }

    public void setWidthMM(Integer widthMM) {
        this.widthMM = widthMM;
    }

    public Integer getLengthMM() {
        return lengthMM;
    }

    public void setLengthMM(Integer lengthMM) {
        this.lengthMM = lengthMM;
    }

    public Integer getHeightMM() {
        return heightMM;
    }

    public void setHeightMM(Integer heightMM) {
        this.heightMM = heightMM;
    }

    public Float getExtraShippingCharge1() {
        return extraShippingCharge1;
    }

    public void setExtraShippingCharge1(Float extraShippingCharge1) {
        this.extraShippingCharge1 = extraShippingCharge1;
    }

    public Float getExtraShippingCharge2() {
        return extraShippingCharge2;
    }

    public void setExtraShippingCharge2(Float extraShippingCharge2) {
        this.extraShippingCharge2 = extraShippingCharge2;
    }

    public Integer getUnitsContained() {
        return unitsContained;
    }

    public void setUnitsContained(Integer unitsContained) {
        this.unitsContained = unitsContained;
    }

    public Integer getPalletTypeId() {
        return palletTypeId;
    }

    public void setPalletTypeId(Integer palletTypeId) {
        this.palletTypeId = palletTypeId;
    }

    public Integer getPackingUnits() {
        return packingUnits;
    }

    public void setPackingUnits(Integer packingUnits) {
        this.packingUnits = packingUnits;
    }

    public Integer getPackingUnitTypeId() {
        return packingUnitTypeId;
    }

    public void setPackingUnitTypeId(Integer packingUnitTypeId) {
        this.packingUnitTypeId = packingUnitTypeId;
    }

    public Float getTransportationCosts() {
        return transportationCosts;
    }

    public void setTransportationCosts(Float transportationCosts) {
        this.transportationCosts = transportationCosts;
    }

    public Float getStorageCosts() {
        return storageCosts;
    }

    public void setStorageCosts(Float storageCosts) {
        this.storageCosts = storageCosts;
    }

    public Float getCustoms() {
        return customs;
    }

    public void setCustoms(Float customs) {
        this.customs = customs;
    }

    public Float getOperatingCosts() {
        return operatingCosts;
    }

    public void setOperatingCosts(Float operatingCosts) {
        this.operatingCosts = operatingCosts;
    }

    public Integer getVatId() {
        return vatId;
    }

    public void setVatId(Integer vatId) {
        this.vatId = vatId;
    }

    public String getBundleType() {
        return bundleType;
    }

    public void setBundleType(String bundleType) {
        this.bundleType = bundleType;
    }

    public Integer getAutomaticClientVisibility() {
        return automaticClientVisibility;
    }

    public void setAutomaticClientVisibility(Integer automaticClientVisibility) {
        this.automaticClientVisibility = automaticClientVisibility;
    }

    public boolean isIsHiddenInCategoryList() {
        return isHiddenInCategoryList;
    }

    public void setIsHiddenInCategoryList(boolean isHiddenInCategoryList) {
        this.isHiddenInCategoryList = isHiddenInCategoryList;
    }

    public Float getDefaultShippingCosts() {
        return defaultShippingCosts;
    }

    public void setDefaultShippingCosts(Float defaultShippingCosts) {
        this.defaultShippingCosts = defaultShippingCosts;
    }

    public String getAvailableUntil() {
        return availableUntil;
    }

    public void setAvailableUntil(String availableUntil) {
        this.availableUntil = availableUntil;
    }

    public String getReleasedAt() {
        return releasedAt;
    }

    public void setReleasedAt(String releasedAt) {
        this.releasedAt = releasedAt;
    }

    public String getPlenty_item_variation_base_show_unit_price() {
        return plenty_item_variation_base_show_unit_price;
    }

    public void setPlenty_item_variation_base_show_unit_price(String plenty_item_variation_base_show_unit_price) {
        this.plenty_item_variation_base_show_unit_price = plenty_item_variation_base_show_unit_price;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public List<AttributeValues> getVariationAttributeValues() {
        ArrayList<AttributeValues> result = new ArrayList();
        result.addAll(variationAttributeValues);
        return result;
    }

    public void setVariationAttributeValues(List<AttributeValues> variationAttributeValues) {
        this.variationAttributeValues = variationAttributeValues;
    }

    public List<SalesPrice> getVariationSalesPrices() {
        ArrayList<SalesPrice> result = new ArrayList();
        result.addAll(variationSalesPrices);
        return result;
    }

    public void setVariationSalesPrices(List<SalesPrice> variationSalesPrices) {
        this.variationSalesPrices = variationSalesPrices;
    }

    public List<Category> getVariationDefaultCategory() {
        ArrayList<Category> result = new ArrayList();
        result.addAll(variationDefaultCategory);
        return result;
    }

    public void setVariationDefaultCategory(List<Category> variationDefaultCategory) {
        this.variationDefaultCategory = variationDefaultCategory;
    }

    public List<Category> getVariationCategories() {
        ArrayList<Category> result = new ArrayList();
        result.addAll(variationCategories);
        return result;
    }

    public void setVariationCategories(List<Category> variationCategories) {
        this.variationCategories = variationCategories;
    }

    public List<Barcode> getVariationBarcodes() {
        List<Barcode> result = new ArrayList();
        result.addAll(variationBarcodes);
        return result;
    }

    public void setVariationBarcodes(List<Barcode> variationBarcodes) {
        this.variationBarcodes = variationBarcodes;
    }

    public List<Market> getVariationMarkets() {
        List<Market> result = new ArrayList();
        result.addAll(variationMarkets);
        return result;
    }

    public void setVariationMarkets(List<Market> variationMarkets) {
        this.variationMarkets = variationMarkets;
    }

    public List<Client> getVariationClients() {
        List<Client> result = new ArrayList();
        result.addAll(variationClients);
        return result;
    }

    public void setVariationClients(List<Client> variationClients) {
        this.variationClients = variationClients;
    }
}
