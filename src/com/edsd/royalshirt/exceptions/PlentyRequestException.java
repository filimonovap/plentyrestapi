package com.edsd.royalshirt.exceptions;

public class PlentyRequestException extends Exception{

    public PlentyRequestException() {
    }

    public PlentyRequestException(String message) {
        super(message);
    }
}
