package com.edsd.royalshirt.exceptions;

public class PlentyValueAlreadyExistException extends Exception{
    public PlentyValueAlreadyExistException() {
    }

    public PlentyValueAlreadyExistException(String message) {
        super(message);
    }
}
