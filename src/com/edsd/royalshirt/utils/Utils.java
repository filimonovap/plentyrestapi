package com.edsd.royalshirt.utils;

import com.edsd.royalshirt.restapidto.Category;
import com.edsd.royalshirt.restapidto.Item;
import com.edsd.royalshirt.restapidto.ItemVariant;
import com.edsd.royalshirt.restapidto.SalesPrice;
import com.edsd.royalshirt.restapidto.Unit;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static Item createNewItem(Item parent){
        Item clone = new Item(parent);
        clone.setId(null);
        clone.setMainVariationId(null);
        return clone;
    }
    
    public static ItemVariant createNewMainItemVariant(ItemVariant parent){   
        ItemVariant clone = createNewBaseItemVariant(parent);
                
        List<Category> cats = new ArrayList();
        Category cat;
        for (Category c : parent.getVariationCategories()) {
            cat = new Category();
            cat.setCategoryId(c.getCategoryId());
            cat.setPosition(c.getPosition());
            cats.add(cat);
        }
        clone.setVariationCategories(cats);
        
        List<SalesPrice> prices = new ArrayList();
        SalesPrice price;
        for(SalesPrice p : parent.getVariationSalesPrices()){
            price = new SalesPrice();
            price.setPrice(p.getPrice()+10);
            price.setSalesPriceId(p.getSalesPriceId());
            prices.add(price);
        }
        clone.setVariationSalesPrices(prices);
        
        return clone;
    }
    
    private static ItemVariant createNewBaseItemVariant(ItemVariant parent){
        ItemVariant clone = new ItemVariant(parent);
        clone.setId(null);
        clone.setItemId(null);
        clone.setNumber(null);

        Unit unit = new Unit();
        unit.setContent(1);
        unit.setUnitId(1);
        clone.setUnit(unit);
        
        return clone;
    }
}
