package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.exceptions.PlentyValueAlreadyExistException;
import com.edsd.royalshirt.restapidto.Property;
import com.edsd.royalshirt.restapidto.PropertyValue;
import com.edsd.royalshirt.restapidto.ValueText;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestApiProperty extends AuthorisedRestApiBase{
    private Gson GSON = new Gson();
    
    public List<PropertyValue> getVariantPropertyValues(Integer itemId, Integer variantId) throws PlentyAuthorizationException, PlentyRequestException {
        List<PropertyValue> result = new ArrayList();
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_properties", ApiProp.PLENTY_SERVER_URL, itemId, variantId);
        String output = getRequest(requestStr, "GET", null);
        
        System.out.println(output);
        
        PropertyValue[] vals = GSON.fromJson(output, PropertyValue[].class);
        result.addAll(Arrays.asList(vals));
        return result;
    }
    
    private void addTextValues(Integer itemId, Integer variantId, Integer propertyId, Integer assocId, List<ValueText> texts){
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_properties/%d/texts", ApiProp.PLENTY_SERVER_URL, itemId, variantId, propertyId);
        for(ValueText t : texts){
            String json = GSON.toJson(t);
            System.out.println(json);
            String output;
            try {
                output = getRequest(requestStr, "POST", json);
                System.out.println(output);
            } catch (PlentyAuthorizationException ex) {
                Logger.getLogger(RestApiProperty.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PlentyRequestException ex) {
                Logger.getLogger(RestApiProperty.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    public void addVariantPropertyValues(Integer itemId, Integer variantId, List<PropertyValue> values){
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_properties", ApiProp.PLENTY_SERVER_URL, itemId, variantId);
        for(PropertyValue val : values){ 
            String json = GSON.toJson(val);
            System.out.println(json);
            String output;
            try {
                output = getRequest(requestStr, "POST", json);
                System.out.println(output);
//                if (val.getValueTexts() != null) {
//                    PropertyValue resVal = GSON.fromJson(output, PropertyValue.class);
//                    List<ValueText> newTexts = new ArrayList();
//                    for (ValueText t : val.getValueTexts()) {
//                        t.setValueId(resVal.getId());
//                        newTexts.add(t);
//                    }
//                    addTextValues(itemId, variantId, val.getPropertyId(), resVal.getId(), newTexts);
//                }
            } catch (PlentyAuthorizationException ex) {
                Logger.getLogger(RestApiProperty.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PlentyRequestException ex) {
                Logger.getLogger(RestApiProperty.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        JsonObject obj = new JsonObject();
//        obj.addProperty("variationId", variantId);
//        obj.addProperty("propertyId", 31);
//       // obj.addProperty("propertySelectionId", 3);
//        obj.addProperty("valueInt", 888);
//        String json = obj.toString();
//        System.out.println(json);
//        String output = getRequest(requestStr, "POST", json);
//        System.out.println(output);
    }
    
    public void deleteProperty(String id) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/properties/%s", ApiProp.PLENTY_SERVER_URL, id);
        getRequest(requestStr, "DELETE", null);
    }

    public String createProperty(String name) throws PlentyAuthorizationException, PlentyRequestException, PlentyValueAlreadyExistException {
        if (isExistProperty(name)) {
            throw new PlentyValueAlreadyExistException("Property with such name already exist!");
        }

        String requestStr = String.format("%s/rest/items/properties", ApiProp.PLENTY_SERVER_URL);
        Property property = new Property();
        property.setBackendName(name);
        property.setValueType("text");

        String output = getRequest(requestStr, "POST", GSON.toJson(property));
        Property p = GSON.fromJson(output, Property.class);
        return p.getId();
    }

    private boolean isExistProperty(String name) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/properties", ApiProp.PLENTY_SERVER_URL);
        JsonObject obj;
        int i = 1;
        boolean isLastPage = false;
        while (!isLastPage) {
            Map<String, String> attr = new HashMap();
            attr.put("page", String.valueOf(i));
            String output = getRequest(requestStr + createAttributeString(attr), "GET", null);
            JsonParser parser = new JsonParser();
            obj = parser.parse(output).getAsJsonObject();
            isLastPage = obj.get("isLastPage").getAsBoolean();
            for (JsonElement e : obj.getAsJsonArray("entries")) {
                if (e == null || e.getAsJsonObject().get("backendName") == null) {
                    continue;
                }
                if (e.getAsJsonObject().get("backendName").getAsString().equalsIgnoreCase(name)) {
                    return true;
                }
            }
            i++;
        }

        return false;
    }

    public List<Property> getProperties() throws PlentyAuthorizationException, PlentyRequestException {
        List<Property> result = new ArrayList();
        String requestStr = String.format("%s/rest/items/properties", ApiProp.PLENTY_SERVER_URL);
        JsonObject obj;
        int i = 1;
        boolean isLastPage = false;
        while (!isLastPage) {
            Map<String, String> attr = new HashMap();
            attr.put("page", String.valueOf(i));
            String output = getRequest(requestStr + createAttributeString(attr), "GET", null);
            JsonParser parser = new JsonParser();
            obj = parser.parse(output).getAsJsonObject();
            isLastPage = obj.get("isLastPage").getAsBoolean();
            Property[] variants = GSON.fromJson(obj.get("entries"), Property[].class);
            result.addAll(Arrays.asList(variants));
            i++;
        }

        return result;
    }
    
    public void createSelection(Integer propertyId, String lang, String value) throws PlentyAuthorizationException, PlentyRequestException, PlentyValueAlreadyExistException {
        if(isSelectionExist(propertyId, lang, value)) throw new PlentyValueAlreadyExistException("Selection with such name already exist!");
        
        String requestStr = String.format("%s/rest/items/properties/%d/selections", ApiProp.PLENTY_SERVER_URL, propertyId);
        JsonObject selection = new JsonObject();
        selection.addProperty("propertyId", propertyId);
        selection.addProperty("lang", lang);
        selection.addProperty("name", value);
        
        String json = selection.toString();
        System.out.println(json);
        
        getRequest(requestStr, "POST", json);
    }
    
    private boolean isSelectionExist(Integer propertyId, String lang, String value) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/properties/%d/selections/%s", ApiProp.PLENTY_SERVER_URL, propertyId, lang);
        
        String output = getRequest(requestStr, "GET", null);
        JsonParser parser = new JsonParser();
        JsonArray vals = parser.parse(output).getAsJsonArray();
        JsonObject o;
        for(JsonElement e : vals){
            o = e.getAsJsonObject();
            String name = o.get("name") != null ? o.get("name").getAsString() : "";
            if(name.equalsIgnoreCase(value)) return true;
        }
        
        return false;
    }
    
}
