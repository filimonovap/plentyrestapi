package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.restapidto.Item;
import com.edsd.royalshirt.restapidto.ItemVariant;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestApiTester{
    private static PlentyRestApiManager API_MANAGER = new PlentyRestApiManager();
    
    public static void main(String[] args) throws InterruptedException {
        try {
//                RestApiItemVariant varApi = new RestApiItemVariant();
//                List<Item> items = varApi.getItems(new String[] {"1000"});
//                ItemVariant var = varApi.getItemMainVariant(1000);
//                System.out.println("FINISH");
                  
                        
                     //System.out.println(API_MANAGER.getRestApiError());
//                     RestApiCategory cApi = new RestApiCategory();
//                     //List<JsonObject> cats = cApi.getCategories();
//                     System.out.println(cApi.addCategory(322, "Super Anton 2"));
                     
                     
                     
                //createItem(new String[]{"1000"});
                //ItemVariant var = API_MANAGER.getItemMainVariant(1000);
                //System.out.println(API_LOG.getRestApiLog());
                //RestApiProperty propApi = new RestApiProperty();
                //propApi.getVariantPropertyValues(1000, var.getId());
                //propApi.addVariantPropertyValues(1177, 3219, propApi.getVariantPropertyValues(1000, var.getId()));
//                System.out.println(API_MANAGER.getRestApiError());
//               API_MANAGER.createItem(new String[] {"1000"});
               RestApiItemVariant varApi = new RestApiItemVariant();
//               List<Item> mItems = varApi.getItems(new String[] {"1000"});
//               List<Item> items = varApi.getItems(new String[] {"1206"});
//               Item item = items.get(0);
////               item.setFlagOne(0);
////               item.setFlagTwo(0);
////               item.setPosition(0);
//               varApi.updateItem(item);
               ItemVariant mVar = varApi.getItemMainVariant(1000);
               ItemVariant newMainVariant = new ItemVariant();
            newMainVariant.setItemId(1213);
            newMainVariant.setId(3276);
            newMainVariant.setPicking(mVar.getPicking() == null ? "" : mVar.getPicking());
            newMainVariant.setReleasedAt(mVar.getReleasedAt());
            newMainVariant.setPlenty_item_variation_base_show_unit_price(mVar.getPlenty_item_variation_base_show_unit_price()); // don't work anyway! 8 from Note
            newMainVariant.setHeightMM(mVar.getHeightMM());
            newMainVariant.setWidthMM(mVar.getWidthMM());
            newMainVariant.setLengthMM(mVar.getLengthMM());
            newMainVariant.setPalletTypeId(mVar.getPalletTypeId());
            newMainVariant.setPackingUnitTypeId(mVar.getPackingUnitTypeId());
            newMainVariant.setCustoms(mVar.getCustoms());
            newMainVariant.setOperatingCosts(mVar.getOperatingCosts());
            newMainVariant.setStockLimitation(222);
            newMainVariant.setBundleType("Kilogramm");
            varApi.updateItemVariant(newMainVariant);
//               ItemVariant var = varApi.getItemMainVariant(1206);
//               var.setReleasedAt(mVar.getReleasedAt());
//               var.setPlenty_item_variation_base_show_unit_price(mVar.getPlenty_item_variation_base_show_unit_price());
//               var.setHeightMM(mVar.getHeightMM());
//               var.setWidthMM(mVar.getWidthMM());
//               var.setLengthMM(mVar.getLengthMM());
//               var.setPalletTypeId(mVar.getPalletTypeId());
//               var.setPackingUnitTypeId(mVar.getPackingUnitTypeId());
//               var.setCustoms(mVar.getCustoms());
//               var.setOperatingCosts(mVar.getOperatingCosts());
//               varApi.updateItemVariant(var);
                
            
        } catch (PlentyAuthorizationException ex) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PlentyRequestException ex) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, ex);
            TimeUnit.SECONDS.sleep(30);
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, API_MANAGER.getRestApiError());
        } catch (Exception e){
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}