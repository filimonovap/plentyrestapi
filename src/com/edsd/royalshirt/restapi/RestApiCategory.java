package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.restapidto.Category;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RestApiCategory extends AuthorisedRestApiBase{
    
    public void addDefaultCategory(Integer itemId, Integer variantId, List<Category> categories) throws PlentyAuthorizationException, PlentyRequestException {
        Gson GSON = new Gson();
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_default_categories", ApiProp.PLENTY_SERVER_URL, itemId, variantId);
        String json;
        for(Category cat : categories){
            json = GSON.toJson(cat);
            System.out.println(json);
            getRequest(requestStr, "POST", json);
        }        
    }
    
    public void deleteCategory(String id) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/categories/%s/clients", ApiProp.PLENTY_SERVER_URL, id);
        getRequest(requestStr, "DELETE", null);
        requestStr = String.format("%s/rest/categories/%s", ApiProp.PLENTY_SERVER_URL, id);
        getRequest(requestStr, "DELETE", null);
    }

    public String addCategory(Integer parentCategoryId, String name) throws PlentyAuthorizationException, PlentyRequestException {
        boolean res = false;
        String requestStr = String.format("%s/rest/categories", ApiProp.PLENTY_SERVER_URL);
        JsonObject category = new JsonObject();
        category.addProperty("type", "item");
        //category.addProperty("linklist", "Y");
        category.addProperty("right", "all");
        category.addProperty("parentCategoryId", parentCategoryId);
        JsonArray detailsArr = new JsonArray();
        JsonObject detailsObj = new JsonObject();
        detailsObj.addProperty("plentyId", ApiProp.PLENTY_ID);
        detailsObj.addProperty("name", name);
        detailsObj.addProperty("lang", ApiProp.DEF_LANG);
        detailsArr.add(detailsObj);
        detailsObj = new JsonObject();
        detailsObj.addProperty("plentyId", 0);
        detailsObj.addProperty("name", name);
        detailsObj.addProperty("lang", "en");
        detailsArr.add(detailsObj);
        category.add("details", detailsArr);
        JsonArray clientsArr = new JsonArray();
        JsonObject clientObj = new JsonObject();
        clientObj.addProperty("plentyId", ApiProp.PLENTY_ID);
        clientsArr.add(clientObj);
        category.add("clients", clientsArr);

        JsonArray categories = new JsonArray();
        categories.add(category);

        System.out.println(categories.toString());

        String output = getRequest(requestStr, "POST", "{" + categories.toString() + "}");

        return output;
    }

    public List<JsonObject> getCategories() throws PlentyAuthorizationException, PlentyRequestException {
        List<JsonObject> result = new ArrayList();
        String requestStr = String.format("%s/rest/categories", ApiProp.PLENTY_SERVER_URL);

        JsonObject obj;
        int i = 1;
        boolean isLastPage = false;
        while (!isLastPage) {
            Map<String, String> attr = new HashMap();
            attr.put("page", String.valueOf(i));
            String output = getRequest(requestStr + createAttributeString(attr), "GET", null);
            System.out.println(output);
            JsonParser parser = new JsonParser();
            obj = parser.parse(output).getAsJsonObject();
            isLastPage = obj.get("isLastPage").getAsBoolean();
            JsonArray categories = obj.getAsJsonArray("entries");
            for (JsonElement o : categories) {
                result.add(o.getAsJsonObject());
            }
            i++;
        }

        return result;
    }
}
