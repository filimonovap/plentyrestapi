package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum AttributeValueNames {
    INSTANCE;
    
    private Map<String, String> names = new HashMap();
    
    public String getValueName(String valueId){
        String name = valueId;
        if(!names.containsKey(valueId)){
            try {
                name = getAttributeValueName(valueId);
                if(name.length() != 0) names.put(valueId, name);
                else name = valueId;
            } catch (PlentyAuthorizationException ex) {
                Logger.getLogger(AttributeValueNames.class.getName()).log(Level.SEVERE, null, ex);
            } catch (PlentyRequestException ex) {
                Logger.getLogger(AttributeValueNames.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            name = names.get(valueId);
        }
        
        return name;
    }
    
    private String getAttributeValueName(String attributeValueId) throws PlentyAuthorizationException, PlentyRequestException{
        String name = "";
        String requestStr = String.format("%s/rest/items/attribute_values/%s/names/%s", ApiProp.PLENTY_SERVER_URL, attributeValueId, ApiProp.DEF_LANG);
        AuthorisedRestApiBase rab = new AuthorisedRestApiBase();
        String output = rab.getRequest(requestStr, "GET", null);
        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(output).getAsJsonObject();
        name = obj.get("name").getAsString();
        
        return name;
    }
}
