package com.edsd.royalshirt.restapi;

public class ApiProp {
   public static final String PLENTY_SERVER_URL = "http://rs.plentymarkets-x3.com";
   public static final String DEF_LANG = "de";
   public static final int PLENTY_ID = 25622;
   public static final int DESIGN_FEATURE = 30;
}
