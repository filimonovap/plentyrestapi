package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.exceptions.PlentyValueAlreadyExistException;
import com.edsd.royalshirt.restapidto.Availabilities;
import com.edsd.royalshirt.restapidto.Barcode;
import com.edsd.royalshirt.restapidto.Category;
import com.edsd.royalshirt.restapidto.Client;
import com.edsd.royalshirt.restapidto.Item;
import com.edsd.royalshirt.restapidto.ItemImage;
import com.edsd.royalshirt.restapidto.ItemText;
import com.edsd.royalshirt.restapidto.ItemVariant;
import com.edsd.royalshirt.restapidto.Market;
import com.edsd.royalshirt.restapidto.Name;
import com.edsd.royalshirt.restapidto.PropertyValue;
import com.edsd.royalshirt.restapidto.Unit;
import com.edsd.royalshirt.restapidto.ValueText;
import com.edsd.royalshirt.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public class PlentyRestApiManager {
    private static List<String> langs = new ArrayList();

    static {
        langs.add("en");
        langs.add("de");
        langs.add("fr");
    }
    
    public void createItem(String[] items) throws PlentyAuthorizationException, PlentyRequestException {
        RestApiItemVariant itemVarApi = new RestApiItemVariant();
        RestApiImages imgApi = new RestApiImages();
        RestApiCategory catApi = new RestApiCategory();
        RestApiProperty propApi = new RestApiProperty();
        
        Unit defaultUnit = new Unit();
        defaultUnit.setContent(1);
        defaultUnit.setUnitId(1);
        
        for (Item item : itemVarApi.getItems(items)) {
            //get base item data ===============================================
            System.out.println("=== get base item data ===");
            List<ItemText> texts = itemVarApi.getItemTexts(item.getId(), item.getMainVariationId(), langs);
            ItemVariant mainVariant = itemVarApi.getItemMainVariant(item.getId());
            List<ItemVariant> variants = itemVarApi.getItemVariants(item.getId());
            List<PropertyValue> properties = propApi.getVariantPropertyValues(item.getId(), mainVariant.getId());
            List<Integer> shippingProfiles = itemVarApi.getItemShippingProfiles(item.getId());

            //create main variant ==============================================
            System.out.println("=== create main variant ===");
            List<ItemVariant> vL = new ArrayList();
            ItemVariant newMainVariant = Utils.createNewMainItemVariant(mainVariant);
            vL.add(newMainVariant);

            //create item ======================================================
            System.out.println("=== create item ===");
            Item resItem = Utils.createNewItem(item);
            resItem.setVariations(vL);
            resItem = itemVarApi.createItem(resItem);
            System.out.println(resItem.getId() + " / " + resItem.getMainVariationId());

            //add shipping profile =============================================
            System.out.println("=== add shipping profile ===");
            itemVarApi.setItemShippingProfiles(resItem.getId(), shippingProfiles);

            //add default category =============================================
            System.out.println("=== add default category ===");
            List<Category> defCats = new ArrayList();
            Category cat;
            for (Category c : mainVariant.getVariationDefaultCategory()) {
                cat = new Category();
                cat.setBranchId(c.getBranchId());
                cat.setPlentyId(c.getPlentyId());
                cat.setManually(c.getManually());
                cat.setVariationId(resItem.getMainVariationId());
                defCats.add(cat);
            }
            catApi.addDefaultCategory(resItem.getId(), resItem.getMainVariationId(), defCats);

            //add clients ======================================================
            System.out.println("=== add clients ===");
            List<Client> clients = new ArrayList();
            Client client;
            for (Client c : mainVariant.getVariationClients()) {
                client = new Client();
                client.setVariantId(resItem.getMainVariationId());
                client.setPlentyId(c.getPlentyId());
                clients.add(client);
            }
            itemVarApi.addClient(resItem.getId(), resItem.getMainVariationId(), clients);

            //add markets ======================================================
            System.out.println("=== add markets ===");
            List<Market> markets = new ArrayList();
            Market market;
            for (Market m : mainVariant.getVariationMarkets()) {
                market = new Market();
                market.setVariantId(resItem.getMainVariationId());
                market.setMarketId(m.getMarketId());
                if (!markets.contains(m)) {
                    markets.add(m);
                }
            }
            itemVarApi.addMarket(resItem.getId(), resItem.getMainVariationId(), markets);
            
            //set unsetteble data for main variant =============================
            System.out.println("=== set unsetteble data for main variant ===");
            newMainVariant = new ItemVariant();
            newMainVariant.setItemId(resItem.getId());
            newMainVariant.setId(resItem.getMainVariationId());
            newMainVariant.setPicking(mainVariant.getPicking() == null ? "" : mainVariant.getPicking());
            newMainVariant.setReleasedAt(mainVariant.getReleasedAt());
            newMainVariant.setPlenty_item_variation_base_show_unit_price(mainVariant.getPlenty_item_variation_base_show_unit_price()); // don't work anyway! 8 from Note
            newMainVariant.setHeightMM(mainVariant.getHeightMM());
            newMainVariant.setWidthMM(mainVariant.getWidthMM());
            newMainVariant.setLengthMM(mainVariant.getLengthMM());
            newMainVariant.setPalletTypeId(mainVariant.getPalletTypeId());
            newMainVariant.setPackingUnitTypeId(mainVariant.getPackingUnitTypeId());
            newMainVariant.setCustoms(mainVariant.getCustoms());
            newMainVariant.setOperatingCosts(mainVariant.getOperatingCosts());
            newMainVariant.setStockLimitation(mainVariant.getStockLimitation());
            newMainVariant.setBundleType("Kilogramm");
            itemVarApi.updateItemVariant(newMainVariant);

            //create item texts ================================================
            System.out.println("=== create item texts ===");
            itemVarApi.addItemTexts(resItem.getId(), resItem.getMainVariationId(), texts);
            
            //upload image =====================================================
            System.out.println("=== upload image ===");
            ItemImage image = new ItemImage();
            image.setUploadUrl("http://kinovopros.com/files/kinovopros/imagecache/i1/i/segodnya/11/02/23/200/samaya-krasivaya-multi-legenda-2010-goda-2838961546.jpg");

            List<Name> names = new ArrayList();
            Name n = new Name();
            n.setLang("de");
            n.setName("Test");
            names.add(n);

            List<Availabilities> availabilities = new ArrayList();
            Availabilities a = new Availabilities();
            a.setType("mandant");
            a.setValue(1);
            availabilities.add(a);

            image.setAvailabilities(availabilities);
            image.setNames(names);

            Integer imgId = imgApi.uploadImage(resItem.getId(), image);

            //create item variants =============================================
            System.out.println("=== create item variants ===");
            Barcode bc = new Barcode();
            bc.setBarcodeId(1);
            bc.setCode("1111111111111");
            List<Barcode> bcs = new ArrayList();
            bcs.add(bc);

            ItemVariant var = new ItemVariant();
            var.setItemId(resItem.getId());
            var.setVariationAttributeValues(variants.get(0).getVariationAttributeValues());
            var.setUnit(defaultUnit);
            var.setIsActive(variants.get(0).isIsActive());
            var.setName(variants.get(0).getName());
            var.setVariationBarcodes(bcs);
            Integer varId = itemVarApi.addItemVariant(var);

            //add image to main variant =============================================
            System.out.println("=== add image to main variant ===");
            imgApi.addImage(resItem.getId(), resItem.getMainVariationId(), imgId);
            
            //add image to variant =============================================
            System.out.println("=== add image to variant ===");
            imgApi.addImage(resItem.getId(), varId, imgId);

            //add properties ===================================================
            System.out.println("=== add properties ===");
            for (PropertyValue val : properties) {
                if (val.getValueTexts() != null) {
                    List<ValueText> newTexts = new ArrayList();
                    for (ValueText t : val.getValueTexts()) {
                        t.setValueId(null);
                        newTexts.add(t);
                    }
                    val.setValueTexts(newTexts);
                }
                val.setVariationId(resItem.getMainVariationId());
                val.setId(null);
            }
            propApi.addVariantPropertyValues(resItem.getId(), resItem.getMainVariationId(), properties);
        }
    }
    
    public void createItemOld(String[] items) throws PlentyAuthorizationException, PlentyRequestException {
        RestApiItemVariant itemVarApi = new RestApiItemVariant();
        RestApiImages imgApi = new RestApiImages();
        RestApiCategory catApi = new RestApiCategory();
        RestApiProperty propApi = new RestApiProperty();
        for (Item item : itemVarApi.getItems(items)) {
            //get base item data ===============================================
            System.out.println("=== get base item data ===");
            List<ItemText> texts = itemVarApi.getItemTexts(item.getId(), item.getMainVariationId(), langs);
            ItemVariant mainVariant = itemVarApi.getItemMainVariant(item.getId());
            List<ItemVariant> variants = itemVarApi.getItemVariants(item.getId());
            List<PropertyValue> properties = propApi.getVariantPropertyValues(item.getId(), mainVariant.getId());
            List<Integer> shippingProfiles = itemVarApi.getItemShippingProfiles(item.getId());

            //create main variant ==============================================
            System.out.println("=== create main variant ===");
            List<ItemVariant> vL = new ArrayList();
            mainVariant.setId(null);
            mainVariant.setItemId(null);
            mainVariant.setNumber(null);
            Unit unit = new Unit();
            unit.setContent(1);
            unit.setUnitId(1);
            mainVariant.setUnit(unit);

            List<Category> cats = new ArrayList();
            for (Category c : mainVariant.getVariationCategories()) {
                Category cat = new Category();
                cat.setCategoryId(c.getCategoryId());
                cat.setPosition(c.getPosition());
                cats.add(cat);
            }
            mainVariant.setVariationCategories(cats);
            vL.add(mainVariant);

            //create item ======================================================
            System.out.println("=== create item ===");
            item.setId(null);
            item.setMainVariationId(null);
            item.setVariations(vL);
            Item resItem = itemVarApi.createItem(item);
            System.out.println(resItem.getId() + " / " + resItem.getMainVariationId());
            
            //add shipping profile =============================================
            System.out.println("=== add shipping profile ===");
            itemVarApi.setItemShippingProfiles(resItem.getId(), shippingProfiles);

            //add default category =============================================
            System.out.println("=== add default category ===");
            List<Category> defCats = new ArrayList();
            for (Category c : mainVariant.getVariationDefaultCategory()) {
                Category cat = new Category();
                cat.setBranchId(c.getBranchId());
                cat.setPlentyId(c.getPlentyId());
                cat.setManually(c.getManually());
                cat.setVariationId(resItem.getMainVariationId());
                defCats.add(cat);
            }
            catApi.addDefaultCategory(resItem.getId(), resItem.getMainVariationId(), defCats);

            //add clients ======================================================
            System.out.println("=== add clients ===");
            List<Client> clients = new ArrayList();
            for (Client c : mainVariant.getVariationClients()) {
                c.setVariantId(resItem.getMainVariationId());
                clients.add(c);
            }
            itemVarApi.addClient(resItem.getId(), resItem.getMainVariationId(), clients);

            //add markets ======================================================
            System.out.println("=== add markets ===");
            List<Market> markets = new ArrayList();
            for (Market m : mainVariant.getVariationMarkets()) {
                m.setVariantId(resItem.getMainVariationId());
                if (!markets.contains(m)) {
                    markets.add(m);
                }
            }
            itemVarApi.addMarket(resItem.getId(), resItem.getMainVariationId(), markets);

            //create item texts ================================================
            System.out.println("=== create item texts ===");
            itemVarApi.addItemTexts(resItem.getId(), resItem.getMainVariationId(), texts);

            //create item variants =============================================
            System.out.println("=== create item variants ===");
            Barcode bc = new Barcode();
            bc.setBarcodeId(1);
            bc.setCode("1111111111111");
            List<Barcode> bcs = new ArrayList();
            bcs.add(bc);

            ItemVariant var = new ItemVariant();
            var.setItemId(resItem.getId());
            var.setVariationAttributeValues(variants.get(0).getVariationAttributeValues());
            Unit u = new Unit();
            u.setContent(1);
            u.setUnitId(1);
            var.setUnit(u);
            //var.setAvailability(variants.get(0).getAvailability());
            var.setIsActive(variants.get(0).isIsActive());
            var.setName(variants.get(0).getName());
            var.setVariationBarcodes(bcs);
            Integer varId = itemVarApi.addItemVariant(var);

            //upload image =====================================================
            System.out.println("=== upload image ===");
            ItemImage image = new ItemImage();
            image.setUploadUrl("http://kinovopros.com/files/kinovopros/imagecache/i1/i/segodnya/11/02/23/200/samaya-krasivaya-multi-legenda-2010-goda-2838961546.jpg");

            List<Name> names = new ArrayList();
            Name n = new Name();
            n.setLang("de");
            n.setName("Test");
            names.add(n);

            List<Availabilities> availabilities = new ArrayList();
            Availabilities a = new Availabilities();
            a.setType("mandant");
            a.setValue(1);
            availabilities.add(a);

            image.setAvailabilities(availabilities);
            image.setNames(names);

            Integer imgId = imgApi.uploadImage(resItem.getId(), image);

            //add image to variant =============================================
            System.out.println("=== add image to variant ===");
            imgApi.addImage(resItem.getId(), varId, imgId);

            //add properties ===================================================
            System.out.println("=== add properties ===");
            for (PropertyValue val : properties) {
                if (val.getValueTexts() != null) {
                    List<ValueText> newTexts = new ArrayList();
                    for (ValueText t : val.getValueTexts()) {
                        t.setValueId(null);
                        newTexts.add(t);
                    }
                    val.setValueTexts(newTexts);
                }
                val.setVariationId(resItem.getMainVariationId());
                val.setId(null);
            }
            propApi.addVariantPropertyValues(resItem.getId(), resItem.getMainVariationId(), properties);
        }
    }
    
    public void createDesignFeature(String value) throws PlentyAuthorizationException, PlentyRequestException, PlentyValueAlreadyExistException{
        RestApiProperty pApi = new RestApiProperty();
        pApi.createSelection(ApiProp.DESIGN_FEATURE, ApiProp.DEF_LANG, value);
    }
    
    public Integer createCategory(String value) throws PlentyAuthorizationException, PlentyRequestException{
        return null;
    }
    
    public String getRestApiError(){
        RestApiLog logApi = new RestApiLog();
        String result = "";
        result = logApi.getLastErrorMessage();
        return result;
    }
}
