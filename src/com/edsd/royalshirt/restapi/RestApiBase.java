package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestApiBase {
    
    protected String getRequest(String urlStr, String type, String auth, String json) throws PlentyRequestException, PlentyAuthorizationException {
        StringBuilder output = new StringBuilder();
        try {
            URL url = new URL(urlStr);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(type);
            if (json != null) {
                conn.setDoOutput(true);
            }
            if (auth != null) {
                conn.setRequestProperty("Authorization", auth);
            }
            conn.setRequestProperty("Content-Type", "application/json");

            if (json != null) {
                OutputStream os = conn.getOutputStream();
                os.write(json.getBytes());
                os.flush();
            }

            if (conn.getResponseCode() != 200) {
                if (conn.getResponseCode() == 401) throw new PlentyAuthorizationException();
                throw new PlentyRequestException("Failed : HTTP error code : " + conn.getResponseCode() + " " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String out;
            while ((out = br.readLine()) != null) {
                output.append(out);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, e);
            throw new PlentyRequestException();
        } catch (IOException e) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, e);
            throw new PlentyRequestException();
        }

        return output.toString();
    }

    protected String createAttributeString(Map<String, String> attrMap) {
        StringBuilder sb = new StringBuilder("?");
        for (Map.Entry elem : attrMap.entrySet()) {
            sb.append(elem.getKey());
            sb.append("=");
            sb.append(elem.getValue());
            sb.append("&");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
