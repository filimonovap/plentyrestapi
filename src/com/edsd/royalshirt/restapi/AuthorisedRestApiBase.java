package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuthorisedRestApiBase extends RestApiBase{            
    protected String getRequest(String urlStr, String type, String json) throws PlentyRequestException, PlentyAuthorizationException{
        String authStr = Authorization.INSTANCE.getAuthorizationString(false);
        try {
            return getRequest(urlStr, type, authStr, json);
        } catch (PlentyAuthorizationException ex) {
            authStr = Authorization.INSTANCE.getAuthorizationString(true);
            return getRequest(urlStr, type, authStr, json);
        }
    }
}
