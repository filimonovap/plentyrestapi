package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.restapidto.Client;
import com.edsd.royalshirt.restapidto.Item;
import com.edsd.royalshirt.restapidto.ItemText;
import com.edsd.royalshirt.restapidto.ItemVariant;
import com.edsd.royalshirt.restapidto.Market;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestApiItemVariant extends AuthorisedRestApiBase{
    private Gson GSON = new Gson();
    
    public void setItemShippingProfiles(Integer itemId, List<Integer> profiles) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/item_shipping_profiles", ApiProp.PLENTY_SERVER_URL, itemId);
        String json;
        for(Integer id : profiles){
            JsonObject obj = new JsonObject();
            obj.addProperty("itemId", itemId);
            obj.addProperty("profileId", id);
            json = obj.toString();
            System.out.println(json);
            getRequest(requestStr, "POST", json);
        }
    }
    
    public List<Integer> getItemShippingProfiles(Integer itemId) throws PlentyAuthorizationException, PlentyRequestException {
        List<Integer> result = new ArrayList();
        String requestStr = String.format("%s/rest/items/%d/item_shipping_profiles", ApiProp.PLENTY_SERVER_URL, itemId);
        String output = getRequest(requestStr, "GET", null);
        JsonParser parser = new JsonParser();
        JsonArray profiles = parser.parse(output).getAsJsonArray();
        JsonObject obj;
        for(JsonElement e : profiles){
            obj = e.getAsJsonObject();
            if(obj.get("profileId") != null){
                result.add(obj.get("profileId").getAsInt());
            }
        }
        return result;
    }

    public void addClient(Integer itemId, Integer variantId, List<Client> clients) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_clients/", ApiProp.PLENTY_SERVER_URL, itemId, variantId);

        String json;
        for (Client c : clients) {
            json = GSON.toJson(c);
            System.out.println(json);
            getRequest(requestStr, "POST", json);
        }
    }

    public void addMarket(Integer itemId, Integer variantId, List<Market> markets) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_markets/", ApiProp.PLENTY_SERVER_URL, itemId, variantId);

        String json;
        for (Market m : markets) {
            json = GSON.toJson(m);
            System.out.println(json);
            getRequest(requestStr, "POST", json);
        }
    }

    public List<ItemText> getItemTexts(Integer itemId, Integer variantId, List<String> langs) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d/descriptions/", ApiProp.PLENTY_SERVER_URL, itemId, variantId);
        List<ItemText> result = new ArrayList();

        for (String lang : langs) {
            System.out.println(requestStr + lang);
            String jsonStr = getRequest(requestStr + lang, "GET", null);
            System.out.println(jsonStr);
            result.add(GSON.fromJson(jsonStr, ItemText.class));
        }

        return result;
    }

    public List<ItemText> getItemTexts(String itemJson) {
        List<ItemText> result = new ArrayList();
        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(itemJson).getAsJsonObject();
        ItemText[] texts = GSON.fromJson(obj.get("texts"), ItemText[].class);
        result.addAll(Arrays.asList(texts));

        return result;
    }

    public void addItemTexts(Integer itemId, Integer variationId, List<ItemText> texts) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d/descriptions", ApiProp.PLENTY_SERVER_URL, itemId, variationId);

        for (ItemText t : texts) {
            System.out.println(requestStr);
            String jsonStr = GSON.toJson(t);
            System.out.println(jsonStr);
            getRequest(requestStr, "POST", jsonStr);
        }
    }
    
    public void updateItem(Item item) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d", ApiProp.PLENTY_SERVER_URL, item.getId());
        String jSonStr = GSON.toJson(item);
        System.out.println(jSonStr);
        String output = getRequest(requestStr, "PUT", jSonStr);
        System.out.println(output);
    }

    public Item createItem(Item item) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items", ApiProp.PLENTY_SERVER_URL);

        String json = GSON.toJson(item);
        System.out.println(json);
        String output = getRequest(requestStr, "POST", json);
        Item resItem = GSON.fromJson(output, Item.class);
        return resItem;
    }

    public List<Item> getItems(String[] ids) throws PlentyAuthorizationException, PlentyRequestException {
        List<Item> result = new ArrayList();
        String requestStr = String.format("%s/rest/items/", ApiProp.PLENTY_SERVER_URL);
        String output;
        for (String id : ids) {
            System.out.println(requestStr);
            output = getRequest(requestStr + id, "GET", null);
            System.out.println(output);
            result.add(GSON.fromJson(output, Item.class));
        }

        return result;
    }
    
    public void updateItemVariant(ItemVariant variant) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d", ApiProp.PLENTY_SERVER_URL, variant.getItemId(), variant.getId());
        String jSonStr = GSON.toJson(variant);
        System.out.println(jSonStr);
        String output = getRequest(requestStr, "PUT", jSonStr);
        System.out.println(output);
    }

    public Integer addItemVariant(ItemVariant variant) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%s/variations", ApiProp.PLENTY_SERVER_URL, variant.getItemId());
        String jSonStr = GSON.toJson(variant);
        System.out.println(jSonStr);
        String output = getRequest(requestStr, "POST", jSonStr);
        ItemVariant resVariant = GSON.fromJson(output, ItemVariant.class);
        return resVariant.getId();
    }

    public List<ItemVariant> getItemVariants(Integer itemId) throws PlentyAuthorizationException, PlentyRequestException {
        List<ItemVariant> result = new ArrayList();
        String requestStr = String.format("%s/rest/items/%d/variations", ApiProp.PLENTY_SERVER_URL, itemId);
        JsonObject obj;
        boolean getOnlyOnePage = false;
        int i = 1;
        boolean isLastPage = false;

        while (!isLastPage) {
            Map<String, String> attr = new HashMap();
            attr.put("with", "variationAttributeValues,variationBarcodes");
            attr.put("isMain", "false");
            attr.put("page", String.valueOf(i));
            String output = getRequest(requestStr + createAttributeString(attr), "GET", null);
            JsonParser parser = new JsonParser();
            obj = parser.parse(output).getAsJsonObject();
            isLastPage = obj.get("isLastPage").getAsBoolean();
            for (JsonElement e : obj.getAsJsonArray("entries")) {
                System.out.println(e.toString());
            }
            ItemVariant[] variants = GSON.fromJson(obj.get("entries"), ItemVariant[].class);
            result.addAll(Arrays.asList(variants));
            i++;
            if (getOnlyOnePage) {
                break;
            }
        }

        Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, "Found " + result.size() + " variants");

        return result;
    }

    public ItemVariant getItemMainVariant(Integer itemId) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%s/variations", ApiProp.PLENTY_SERVER_URL, itemId);
        JsonObject obj;

        Map<String, String> attr = new HashMap();
        attr.put("with", "variationDefaultCategory,variationSalesPrices,variationCategories,variationMarkets,variationClients");
        attr.put("isMain", "true");
        String output = getRequest(requestStr + createAttributeString(attr), "GET", null);
        JsonParser parser = new JsonParser();
        obj = parser.parse(output).getAsJsonObject();
        for (JsonElement e : obj.getAsJsonArray("entries")) {
            System.out.println(e.toString());
        }
        ItemVariant[] variants = GSON.fromJson(obj.get("entries"), ItemVariant[].class);

        return variants.length > 0 ? variants[0] : null;
    }
}
