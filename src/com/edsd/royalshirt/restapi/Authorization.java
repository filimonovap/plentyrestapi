package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.google.gson.Gson;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum Authorization{
    INSTANCE;
    
    private String AUTH;
    {
        this.AUTH="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImI1NGQzZTg1YmFiZmIxODc5YWY4ZTI3Yjc0MTcyNGJmNjAzZWE4ZmU1ZWYyNGRiMWM4ZTFkMzAyNjc3YzgxYTZhMDY2NGUyYWE1MDViMzkxIn0.eyJhdWQiOiIxIiwianRpIjoiYjU0ZDNlODViYWJmYjE4NzlhZjhlMjdiNzQxNzI0YmY2MDNlYThmZTVlZjI0ZGIxYzhlMWQzMDI2NzdjODFhNmEwNjY0ZTJhYTUwNWIzOTEiLCJpYXQiOjE0OTU2MTM1OTUsIm5iZiI6MTQ5NTYxMzU5NSwiZXhwIjoxNDk1Njk5OTk1LCJzdWIiOiIyIiwic2NvcGVzIjpbIioiXX0.UblCluvM_BThLnEIn5b6lBDLAZbv6qmrPKB9Erfp4PocOJCd3Rx3Z8GtzKtAc-53JRZ4lanundJTX3VemOzrTcqJe-sKKdYFkEnlhQ65eAjRvX16eAIDeb3x8fZ_yliPbSmta7BCuQvQaEXwvuE32_7ccYUr1HhiYM96F9tqLx8kLrdFivhmKOFCo6xK8pg51UyxTG0Xp49kmnzFh6W-L9CqJC2E2gMtDaz-OA0pJ4crtp6pZJgtVqT6TSdf8f9ylub1WWi61om1bgiBrKGlLssSV-vKkNivrmqK0KoPOx7ivC6pgHtjeUiGyJDwpitWX4xMK6wVoULfn_uD1QJPYN-1-NjGXOqzbpDN0PNR48L97IPP2-fqfWATnyh4qJq6qO6g-HAP44rTjTXOQZk8eyhO627NUKj_3FB9fgZvA2wbjIgufjWuXauOFMB2l9WIsqlF1zvfxz3S4AVuNm9ZhgVYrQJBMHKT6NBARq1bmZHKOoHs1MfcCbxFNdLNk9CD1x3cdKaTA4ahZt7X4KsvBllkMhi5QUBrEzCWqC05Ta36Nc1ONLAGXjkAcl94l88aEIoYsn2XY15tWBAZ3_zaSifdfkAZiZi37hR2bquOTZid2CSIMGHbHId2JHGssH8qtKGR-naIND3vO4FGgsiuRQ570aGL-IH8B_nfvPZ1zQw";
    };
    
    String getAuthorizationString(boolean getNew) throws PlentyAuthorizationException {
        if (AUTH == null || AUTH.length() == 0 || getNew) {
            try {
                Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, "Authorization string is empty. Get Authorization string...");
                RestApiBase rab = new RestApiBase();
                String toExtract = rab.getRequest(String.format("%s/rest/login?username=%s&password=%s", ApiProp.PLENTY_SERVER_URL, "tester", "RShirt6584!"), "POST", null, null);
                Gson GSON = new Gson();
                Properties data = GSON.fromJson(toExtract, Properties.class);
                AUTH = data.getProperty("tokenType") + " " + data.getProperty("accessToken");
                
                System.out.println(AUTH);
                
            } catch (PlentyRequestException ex) {
                Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, "Authorization problem!");
                throw new PlentyAuthorizationException();
            }
        }
        return AUTH;
    }
}
