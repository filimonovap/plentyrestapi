package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.edsd.royalshirt.restapidto.ItemImage;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class RestApiImages extends AuthorisedRestApiBase{  
    private Gson GSON = new Gson();
    
    public Map<String, ItemImage> getItemImages(String itemId) throws PlentyAuthorizationException, PlentyRequestException {
        Map<String, ItemImage> result = new HashMap();
        String requestStr = String.format("%srest/items/%s/images", ApiProp.PLENTY_SERVER_URL, itemId);
        String output = getRequest(requestStr, "GET", null);
        ItemImage[] images = GSON.fromJson(output, ItemImage[].class);
        for (ItemImage image : images) {
            result.put(image.getId(), image);
        }
        return result;
    }

    public Map<String, String> getItemVariantImages(String itemId) throws PlentyAuthorizationException, PlentyRequestException {
        Map<String, String> result = new HashMap();
        String requestStr = String.format("%s/rest/items/%s/variation_images", ApiProp.PLENTY_SERVER_URL, itemId);
        String output = getRequest(requestStr, "GET", null);
        Properties[] props = GSON.fromJson(output, Properties[].class);
        for (Properties prop : props) {
            result.put(prop.getProperty("variationId"), prop.getProperty("imageId"));
        }
        return result;
    }
    
    public Integer uploadImage(Integer itemId, ItemImage image) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/images/upload", ApiProp.PLENTY_SERVER_URL, itemId);
        String jSonStr = GSON.toJson(image);
        System.out.println(jSonStr);
        String output = getRequest(requestStr, "POST", jSonStr);
        JsonParser parser = new JsonParser();
        JsonObject obj = parser.parse(output).getAsJsonObject();
        return obj.get("id").getAsInt();
    }
    
    public void addImage(Integer itemId, Integer variantId, Integer imageId) throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/items/%d/variations/%d/variation_images", ApiProp.PLENTY_SERVER_URL, itemId, variantId);
        JsonObject obj = new JsonObject();
        obj.addProperty("imageId", imageId);
        obj.addProperty("itemId", itemId);
        obj.addProperty("variationId", variantId);
        String jSonStr = obj.toString();
        System.out.println(jSonStr);
        getRequest(requestStr, "POST", jSonStr);
    }
}
