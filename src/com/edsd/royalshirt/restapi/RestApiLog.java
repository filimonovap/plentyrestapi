package com.edsd.royalshirt.restapi;

import com.edsd.royalshirt.exceptions.PlentyAuthorizationException;
import com.edsd.royalshirt.exceptions.PlentyRequestException;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RestApiLog extends AuthorisedRestApiBase{
    
    public String getLastErrorMessage() {
        try {
            String logJsonStr = getRestApiLog();
            if (logJsonStr != null && logJsonStr.length() != 0) {
                JsonParser parser = new JsonParser();
                JsonObject obj = parser.parse(logJsonStr).getAsJsonObject();
                JsonArray objArr = obj.getAsJsonArray("entries");
                if (objArr.size() == 0) {
                    return "";
                }
                if(objArr == null || objArr.get(0) == null || (objArr.get(0).getAsJsonObject().get("additionalInfo") == null && objArr.get(0).getAsJsonObject().get("codeMessage") == null) || objArr.get(0).getAsJsonObject().get("createdAt") == null){
                    return "ERROR: Can't get last error message! \n Full log:\n" + logJsonStr;
                }

                String error = (objArr.get(0).getAsJsonObject().get("additionalInfo") != null ? objArr.get(0).getAsJsonObject().get("additionalInfo").getAsString() : objArr.get(0).getAsJsonObject().get("codeMessage").getAsString()) + 
                                " : " + objArr.get(0).getAsJsonObject().get("createdAt").getAsString();
                return error != null ? error : "";
            }
            return "";
        } catch (PlentyAuthorizationException ex) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, ex);
            return "Can't get error message, Authorization problem!";
        } catch (PlentyRequestException ex) {
            Logger.getLogger(PlentyRestApiManager.class.getName()).log(Level.SEVERE, null, ex);
            return "Can't get error message! Plese look logs for details!";
        }
    }

    public String getRestApiLog() throws PlentyAuthorizationException, PlentyRequestException {
        String requestStr = String.format("%s/rest/logs", ApiProp.PLENTY_SERVER_URL);
        String output = getRequest(requestStr, "GET", null);
        return output;
    }
}
